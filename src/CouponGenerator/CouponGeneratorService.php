<?php

namespace Dracoder\CouponGenerator;

use Exception;

class CouponGeneratorService {

    private string $charset = '23456789ABCDEFGHJKMNPQRSTUVWXYZ';
    private int $charsetLength;
    private string $separator = '-';
    private int $numberOfBlocks = 3;
    private int $blockLength = 4;

    public function __construct(
        ?int $numberOfBlocks = null,
        ?int $blockLength = null,
        ?string $separator = null,
        ?string $charset = null
    ) {
        if ($numberOfBlocks) {
            $this->numberOfBlocks = $numberOfBlocks;
        }
        if ($blockLength) {
            $this->blockLength = $blockLength;
        }
        if ($separator !== null) {
            $this->separator = $separator;
        }
        if ($charset !== null) {
            $this->charset = $charset;
        }
        $this->charsetLength = strlen($this->charset);
    }

    /**
     * @return int
     */
    public function getNumberOfBlocks(): int
    {
        return $this->numberOfBlocks;
    }

    /**
     * @param int $numberOfBlocks
     *
     * @return $this
     */
    public function setNumberOfBlocks(int $numberOfBlocks): self
    {
        $this->numberOfBlocks = $numberOfBlocks;

        return $this;
    }

    /**
     * @return int
     */
    public function getBlockLength(): int
    {
        return $this->blockLength;
    }

    /**
     * @param int $blockLength
     *
     * @return $this
     */
    public function setBlockLength(int $blockLength): self
    {
        $this->blockLength = $blockLength;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     *
     * @return $this
     */
    public function setSeparator(string $separator): self
    {
        $this->separator = $separator;

        return $this;
    }
    /**
     * @return string
     */
    public function getCharset(): string
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     *
     * @return $this
     */
    public function setCharset(string $charset): self
    {
        $this->charset = $charset;
        $this->charsetLength = strlen($this->charset);

        return $this;
    }

    /**
     * @param string|null $prefix
     * @param array $existingNames
     *
     * @return string|null
     */
    public function generateCouponName(
        array $existingNames = [],
        ?string $prefix = null
    ): ?string {
        $couponName = null;
        while (!$couponName || in_array($couponName, $existingNames, true)) {
            try {
                if ($prefix) {
                    $couponName = $prefix.$this->separator;
                    $currentBlocks = 1;
                } else {
                    $couponName = '';
                    $currentBlocks = 0;
                }
                for ($i = $currentBlocks; $i < $this->numberOfBlocks; $i++) {
                    $block = $this->getBlock();
                    $couponName .= $this->separator.$block;
                }
            } catch (Exception $e) {
            }
        }

        return $couponName;
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    private function getBlock(): string
    {
        $block = '';
        for ($i = 0; $i < $this->blockLength; $i++) {
            $char = random_int(0, $this->charsetLength - 1);
            $block .= $this->charset[$char];
        }

        return $block;
    }
}